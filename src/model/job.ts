export interface Job {
  pattern: string;
  queue: string;
  message: string;
}
