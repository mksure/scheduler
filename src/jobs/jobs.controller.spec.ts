import { Test, TestingModule } from '@nestjs/testing';
import { INestApplication } from '@nestjs/common';
import { JobsController } from './jobs.controller';

describe('JobsController', () => {
  let app: TestingModule;

  beforeAll(async () => {
    app = await Test.createTestingModule({
      controllers: [JobsController],
    }).compile();
  });

  describe('findAll', () => {
    const result = [
      {
        pattern: '*',
        queue: 'voipms',
        message: 'getBalance',
      },
    ];
    it('should return "Hello World!"', () => {
      const appController = app.get<JobsController>(JobsController);
      expect(appController.findAll()).toEqual(result);
    });
  });
});
