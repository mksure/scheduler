import { Controller, Get } from '@nestjs/common';
import { Job } from 'model/job';

@Controller('jobs')
export class JobsController {
  @Get()
  findAll(): Job[] {
    return [
      {
        pattern: '*',
        queue: 'voipms',
        message: 'getBalance',
      },
    ];
  }
}
