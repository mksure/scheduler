import { JobsController } from './jobs.controller';
import { Module } from '@nestjs/common';

@Module({
  controllers: [JobsController],
})
export class JobsModule {}
